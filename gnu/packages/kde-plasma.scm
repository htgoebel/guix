;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2017,2018 Hartmut Goebel <h.goebel@crazy-compilers.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gnu packages kde-plasma)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system trivial)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages bootloaders)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages kde)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (gnu packages libcanberra)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages iso-codes)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages openbox)
  #:use-module (gnu packages password-utils)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages pulseaudio)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xdisorg)
  #:use-module (gnu packages xorg))


(define-public bluedevil
  (package
    (name "bluedevil")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/bluedevil-" version ".tar.xz"))
      (sha256
       (base32 "0am708cb6jfccx1jfbriwc2jgwd4ajqllirc9i0bg4jz5ydxbjxg"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("bluez-qt" ,bluez-qt)
       ("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("kded" ,kded)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("knotifications" ,knotifications)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("plasma-framework" ,plasma-framework)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("shared-mime-info" ,shared-mime-info)
       ;; file kservicetypes5/kfileitemaction-plugin.desktop"
       ;; Required for property type definitions
       ("kcmutils" ,kcmutils) ;; defines only some of the properties
       ;; run-time dependencies
       ("bluez" ,bluez)
       ("pulseaudio" ,pulseaudio))) ;; more specific: pulseaudio-module-bluetooth
    (home-page "https://cgit.kde.org/bluedevil.git")
    (synopsis "KDE Plasma Bluetooth stack")
    (description "BlueDevil is a set of components which integrate Bluetooth
in KDE.  It contains:
@itemize
@item A KDE Control Module (KCM) to configure all the Bluetooth-related
      options.
@item Integration with the KDE input/output system (KIO), which allows you to
      discover and explore Bluetooth devices from your favorite file browser.
@item A wizard to pair your devices and connect directly to services they
      offer, such as input (mouse, keyboard, Wiimote) and audio (headsets,
      phones).
@item A system tray application from where all BlueDevil actions can be done
      (disconnect devices, send files, configure, etc).
@item A daemon which listens to incoming requests, for example to receive
      files or to introduce a requested PIN.
@end itemize")
    (license license:gpl3))) ;; KDE e.V.

(define-public breeze
  (package
    (name "breeze")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/breeze-" version ".tar.xz"))
      (sha256
       (base32 "09jkkfdmngvbp8i2y6irlv6yvrzpc86mw6apmqvphiaqsilyxaw0"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)))
    ;; TODO: Warning at /gnu/store/…-kpackage-5.34.0/…/KF5PackageMacros.cmake:
    ;;   warnings during generation of metainfo for org.kde.breezedark.desktop:
    ;;   Package type "Plasma/LookAndFeel" not found
    ;; TODO: Unknown property type for key "X-KDE-ParentApp",
    ;;    "X-Plasma-MainScript"
    (inputs
     `(("kcmutils" ,kcmutils)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kde-frameworkintegration" ,kde-frameworkintegration)
       ("kdecoration" ,kdecoration)
       ("kguiaddons" ,kguiaddons)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kpackage" ,kpackage)
       ("kwayland" ,kwayland)
       ("kwindowsystem" ,kwindowsystem)
       ("plasma-framework" ,plasma-framework) ; missing in CMakeList.txt
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtx11extras" ,qtx11extras)))
    (home-page "https://www.kde.org/plasma-desktop")
    (synopsis "Default Plasma theme (meta-package)")
    (description "Artwork, styles and assets for the Breeze visual style for
the Plasma Desktop")
    (license license:gpl2+)))

(define-public breeze-grub
  (package
    (name "breeze-grub")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/breeze-grub-" version ".tar.xz"))
      (sha256
       (base32 "03hsq77gi75chgyq9pzh3ry6k6bi78pfm33zn8gx784k9fx7gvqr"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system trivial-build-system)
    (arguments
    `(#:modules ((guix build utils))
      #:builder
        (begin
          (use-modules (guix build utils))
          (let ((theme-dir (string-append %output "/grub/themes/breeze"))
                (tar      (string-append (assoc-ref %build-inputs "tar")
                                          "/bin/tar"))
                (PATH     (string-append (assoc-ref %build-inputs "xz")
                                          "/bin")))
            (setenv "PATH" PATH)
            (system* tar "xv" "--strip-components=1"
                     "-f" (assoc-ref %build-inputs "source"))
            (mkdir-p theme-dir)
            (system* "sh" "./mkfont.sh")
            (copy-recursively "breeze" theme-dir)))))
    (native-inputs
     `(("grub" ,grub)
       ("tar" ,tar)
       ("xz" ,xz)))
    (inputs
     `(("font-gnu-unifont" ,font-gnu-unifont)))
    (home-page "https://www.kde.org/plasma-desktop")
    (synopsis "'Breeze' theme for grub")
    (description "'Breeze' theme for grub")
    (license license:gpl3+)))

;; (define-public breeze-plymouth
;;   (package
;;     (name "breeze-plymouth")
;;     (version "5.9.3")
;;     (source
;;      (origin
;;       (method url-fetch)
;;       (uri (string-append "mirror://kde/stable/plasma/" version
;;                           "/breeze-plymouth-" version ".tar.xz"))
;;       (sha256
;;        (base32 "0vz0hgx3hl03h4rkfrcaydaddljam3jbg6gd7n600a068p74s2mm"))))
;;     ;; /gnu/store/n4nmkwvjxd1mjcl91h537m9y0h5gfv9x-breeze-plymouth-5.9.3.tar.xz
;;     (properties `((tags . '("Desktop" "KDE" "Plasma"))))
;;     (build-system trivial-build-system)
;;     (arguments
;;      `(#:builder #f))
;;     ;; TODO: GuixSD branding, see README in archive. Maybe replace the
;;     ;; os.log.{png,svgz}
;;     (native-inputs
;;      `(("extra-cmake-modules" ,extra-cmake-modules)))
;;     ;;    (inputs
;;     ;;     `("plymouth" ,#f))
;;     (home-page "https://www.kde.org/plasma-desktop")
;;     (synopsis "'Breeze' theme for Plymouth")
;;     (description "'Breeze' theme for Plymouth")
;;     (license license:gpl2+)))


(define-public breeze-gtk
  (package
    (name "breeze-gtk")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/breeze-gtk-" version ".tar.xz"))
      (sha256
       (base32 "1knh0b27b81rnd87s31s2mawqcl1yzwjcakk5npzfm3nj23xakv3"))))
    ;; TODO: move to gnome.scm?
    (properties `((tags . '("Desktop" "GTK+"))))
    (build-system cmake-build-system)
    (arguments
     '(#:tests? #f)) ; No 'test' target
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("qtbase" ,qtbase)))
    ;; TODO: run-time dependency: GTKEngine (required for GTK 2 theme)
    (home-page "https://www.kde.org/plasma-desktop")
    (synopsis "GTK+ theme matching KDE's Breeze theme")
    (description "A GTK+ theme created to match with the Plasma 5 Breeze
theme.

To set the theme in Plasma 5, install kde-gtk-config and use System Settings >
Application Style > GNOME Application Style.  Also make sure to disable “apply
colors to non-Qt applications“ in System Settings > Colors > Options.")
    (license license:lgpl2.1+)
    (properties '((upstream-name . "breeze-gtk")))))

(define-public discover
  (package
    (name "discover")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/discover-" version ".tar.xz"))
      (sha256
       (base32 "1q3nc5lih95vs5masd8z897hvfvpwidiisj8bg62iq0cblsgwz6d"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f ;; FIXME
       #:validate-runpath? #f ;; FIXME
       #:configure-flags
       (let* ((out (assoc-ref %outputs "out"))
              (lib (assoc-ref %outputs "out"))
              (libdir (string-append lib "/lib")))
         (list ;(string-append "-DCMAKE_INSTALL_PREFIX=" out)
               ;(string-append "-DCMAKE_INSTALL_LIBDIR=" libdir)
               ;; We need both libdir and libdir/ceph in RUNPATH.
               (string-append "-DCMAKE_INSTALL_RPATH="
                              libdir ";" libdir "/plasma-discover")
;               (string-append "-DCMAKE_INSTALL_SYSCONFDIR=" out "/etc")
;               (string-append "-DCMAKE_INSTALL_MANDIR=" out "/share/man")
;               (string-append "-DCMAKE_INSTALL_DOCDIR=" out "/share/ceph/doc")
;               (string-append "-DCMAKE_INSTALL_LIBEXECDIR=" out "/libexec")
;               "-DALLOCATOR=jemalloc"
))
       #:phases
       (modify-phases %standard-phases
         (add-before 'check 'check-setup
           (lambda _
             ;; make Qt render "offscreen", required for tests
             (setenv "QT_QPA_PLATFORM" "offscreen")
             #t)))))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("attica" ,attica)
       ("karchive" ,karchive)
       ("kconfig" ,kconfig)
       ("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("kdeclarative" ,kdeclarative)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("kitemmodels" ,kitemmodels)
       ("knewstuff" ,knewstuff)
       ("knotifications" ,knotifications)
       ("kxmlgui" ,kxmlgui)
       ("plasma-framework" ,plasma-framework)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ;; run-time packages
       ("kirigami", kirigami)
       ;; optional packages
       ;;packagekitqt5 http://www.packagekit.org
       ;;AppStreamQt Library that lists Appstream resources
))
    (home-page "https://www.kde.org/plasma-desktop")
    (synopsis "Application Installer for applications delivered as AppStream packages") ; correct?
    (description "KDE and Plasma resources management GUI.

This most probably is not of much use in GuixSD for now.")
    (license (list license:lgpl2.0+ license:gpl3+ license:fdl1.2+))))

(define-public kactivitymanagerd
  (package
    (name "kactivitymanagerd")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kactivitymanagerd-" version ".tar.xz"))
      (sha256
       (base32 "0zfvypxh748vsl270l8wn6inmp8shi2m051yy699qdqbyb039wjq"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("boost" ,boost)
       ("kconfig" ,kconfig)
       ("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("kglobalaccel" ,kglobalaccel)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("kwindowsystem", kwindowsystem)
       ("kxmlgui", kxmlgui)
       ("qtbase" ,qtbase)))
    (home-page "https://cgit.kde.org/kactivitymanagerd.git/")
    (synopsis "System service to manage user's activities, track the usage patterns etc")
    (description "When a user is interacting with a computer, there are three
main areas of contextual information that may affect the behaviour of the
system: who the user is, where they are, and what they are doing.

*Activities* deal with the last one.  An activity might be \"developing a KDE
application\", \"studying 19th century art\", \"composing music\" or
\"watching funny videos\".  Each of these activites may involve multiple
applications, and a single application may be used in multiple activities (for
example, most activities are likely to involve using a web browser, but
different activities will probably involve different websites).

KActivities provides the infrastructure needed to manage a user's activites,
allowing them to switch between tasks, and for applications to update their
state to match the user's current activity.  This includes a daemon, a library
for interacting with that daemon, and plugins for integration with other
frameworks.")
    (license license:gpl3+)))

(define-public kde-cli-tools
  (package
    (name "kde-cli-tools")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kde-cli-tools-" version ".tar.xz"))
      (sha256
       (base32 "0p1az420p4ldinmxnkdwl69542ddm0r4f3wmdysfird7d68yw2hp"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f)) ; TODO: 1/1 fails – propably due to search-path errors
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kactivities" ,kactivities)
       ("kcmutils" ,kcmutils)
       ("kconfig" ,kconfig)
       ("kdesu" ,kdesu)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kinit" ,kinit)
       ("kio" ,kio)
       ("kwindowsystem" ,kwindowsystem)
       ("qtbase" ,qtbase)
       ("qtsvg" ,qtsvg)
       ("qtx11extras" ,qtx11extras)))
    (home-page "https://cgit.kde.org/kde-cli-tools.git")
    (synopsis "Tools to use KDE services from the command line")
    (description "These command line tools enable you to use KDE services such
as kioslaves, kdesu, QtSVG, kcontrol modules, KDE trader and start
applications from the command line.")
    (license (list license:gpl3+ license:gpl2)))) ;; KDE e.V.

(define-public kdecoration
  (package
    (name "kdecoration")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kdecoration-" version ".tar.xz"))
      (sha256
       (base32 "04p77fs5c9b4mbpcl4a2c1wc0i09g51b7c1v7n9fd4nfkm7z8sqs"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("ki18n" ,ki18n)
       ("qtbase" ,qtbase)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'check 'check-setup
           (lambda _
             ;; make Qt render "offscreen", required for tests
             (setenv "QT_QPA_PLATFORM" "offscreen")
             #t)))))
    (home-page "https://cgit.kde.org/kdecoration.git")
    (synopsis "Plugin based library to create window decorations")
    (description "KDecoration is a library to create window decorations.
These window decorations can be used by for example an X11 based window
manager which re-parents a Client window to a window decoration frame.")
    (license license:lgpl3+))) ;; KDE e.V.

(define-public kdeplasma-addons
  (package
    (name "kdeplasma-addons")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kdeplasma-addons-" version ".tar.xz"))
      (sha256
       (base32 "1a4f61bbwhc2y0lnrglbq3sas16bxff0ga3im9d15nq5a5q637i1"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kactivities" ,kactivities)
       ("kcmutils" ,kcmutils)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kdelibs4support" ,kdelibs4support)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("knewstuff" ,knewstuff)
       ("kross" ,kross)
       ("krunner" ,krunner)
       ("kservice" ,kservice)
       ("kunitconversion" ,kunitconversion)
       ("plasma-framework" ,plasma-framework)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtscript" ,qtscript)
       ("plasma-workspace" ,plasma-workspace)
       ("qtx11extras" ,qtx11extras)))
    (home-page "https://techbase.kde.org/Projects/Plasma/Plasmoids")
    (synopsis "Additionsl applets and engines for KDE Plasma")
    (description "Kdeplasma is a compilation of Plasma add-ons (runners,
applets, widgets, wallpappers, plasmoids, etc.)")
    (license (list license:gpl2+ license:lgpl2.0+))))


(define-public kgamma5
  (package
    (name "kgamma5")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kgamma5-" version ".tar.xz"))
      (sha256
       (base32 "08brmdi5y69iwhj7506q2l0bfm92c9l9ds9w4d1ipcgnbydrhfyn"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("ki18n" ,ki18n)
       ("qtbase" ,qtbase)))
    (home-page "http://www.kde.org/")
    (synopsis "Monitor calibration panel for KDE")
    (description "This package contains a settings panel for adjusting the
brightness, contrast, and gamma-correction of a monitor.  Test patterns are
shown to help determine the settings that accurately display the full range of
colors.

Each of the red, green, and blue components can be adjusted individually, or
all three components can be adjusted together.")
    (license license:gpl2+)))

(define-public khotkeys
  (package
    (name "khotkeys")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/khotkeys-" version ".tar.xz"))
      (sha256
       (base32 "16kp5ck6zfpnmnvspdnqklix54np3sxvj5ixs9saqf3gd5rk49mp"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kcmutils" ,kcmutils)
       ("kdbusaddons" ,kdbusaddons)
       ("kdelibs4support" ,kdelibs4support)
       ("kglobalaccel" ,kglobalaccel)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("kxmlgui" ,kxmlgui)
       ("plasma-framwork" ,plasma-framework)
       ("plasma-workspace" ,plasma-workspace)
       ("qtx11extras" ,qtx11extras)))
    (home-page "https://cgit.kde.org/khotkeys.git")
    (synopsis "Configure input actions for KDE Plasma")
    (description "KHotKeys is core-part of the KDE Plasma desktop.")
    (license license:gpl2+)))

(define-public kmenuedit
  (package
    (name "kmenuedit")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kmenuedit-" version ".tar.xz"))
      (sha256
       (base32 "0zha39cd3p5nmrbkhkbcavxns2n2wnb6chc5kcsk5km9wn4laxz0"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kdbusaddons" ,kdbusaddons)
       ("kded" ,kded) ; required for property type definitions
       ("kdelibs4support" ,kdelibs4support)
       ("khotkeys", khotkeys) ; optional
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kxmlgui" ,kxmlgui)
       ("plasma-framework" ,plasma-framework)
       ("qtbase" ,qtbase)
       ("sonnet" ,sonnet)))
    (home-page "https://cgit.kde.org/kmenuedit.git")
    (synopsis "Plasma XDG Menu Editor")
    (description "This package provides a menu editor which may be used to
edit the KDE Plasma workspaces menu or any other XDG menu.")
    (license (list license:gpl2+ license:gpl2))))

(define-public kscreen
  (package
    (name "kscreen")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kscreen-" version ".tar.xz"))
      (sha256
       (base32 "0kf1cf88n46b4js7x9r504605v68wp5hwpwid6phvfqdyqrvbb77"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'check 'check-setup
           (lambda _
             (setenv "HOME" (getcwd))
             (setenv "DBUS_FATAL_WARNINGS" "0")
             ;; The test suite requires a running X server.
             (system "Xvfb :98 -screen 0 640x480x24 &")
             (setenv "DISPLAY" ":98")
             #t)))))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("xorg-server" ,xorg-server))) ; required for the tests
    (inputs
     `(("kauth" ,kauth)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("kdeclarative" ,kdeclarative)
       ("kded" ,kded) ;; missing in CMakeList.txt
       ("kglobalaccel" ,kglobalaccel)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kxmlgui" ,kxmlgui)
       ("libkscreen" ,libkscreen)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)))
    (home-page "https://projects.kde.org/projects/extragear/base/kscreen")
    (synopsis "KDE monitor hotplug and screen handling")
    (description "The KDE multiple monitor support is trying to be as smart as
possible adapting the behavior of it to each use case making the configuration
of monitors as simple as plugging them to your computer.

This package contains the modules and plugins for monitor hot-plugging and
automatic screen handling.")
    (license license:lgpl2.1+)))

(define-public kscreenlocker
  (package
    (name "kscreenlocker")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kscreenlocker-" version ".tar.xz"))
      (sha256
       (base32 "171zjk9r333kbkb9pashw0rdmiwq11nzfin4wnmqzwp7rrclxs18"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'check)
         (add-after 'install 'check
           (lambda _
-             ;; Be patient, some of the tests take up to 100 sec.
             (invoke "dbus-launch" "ctest" ".")))
         (add-before 'check 'check-setup
           (lambda _
             (setenv "HOME" (getcwd))
             (setenv "DBUS_FATAL_WARNINGS" "0")
             (setenv "CTEST_OUTPUT_ON_FAILURE" "1")
             ;; The test suite requires a running X server, setting
             ;; QT_QPA_PLATFORM=offscreen does not suffice and even makes
             ;; some tests fail.
             (system "Xvfb :98 -screen 0 640x480x24 &")
             (setenv "DISPLAY" ":98")
             #t)))))
    (native-inputs
     `(("dbus" ,dbus) ; required for running the tests
       ("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)
       ("xorg-server" ,xorg-server))) ; required for running the tests
    (inputs
     ;; TODO: ConsoleKit (recommended) for emergency unlock
     `(("kcmutils" ,kcmutils)
       ("kcrash" ,kcrash)
       ("kdeclarative" ,kdeclarative)
       ("kglobalaccel" ,kglobalaccel)
       ("ki18n" ,ki18n)
       ;; TODO:? "Could not set up the appstream test. appstreamcli is missing."
       ("kidletime" ,kidletime)
       ("knotifications" ,knotifications)
       ("ktextwidgets" ,ktextwidgets)
       ("kwayland" ,kwayland)
       ("kwindowsystem" ,kwindowsystem)
       ("kxmlgui" ,kxmlgui)
       ("libseccomp" ,libseccomp) ; for sandboxing the look'n'feel package
       ("libxcursor" ,libxcursor) ; missing in CMakeList.txt
       ("libxi" ,libxi) ; XInput, required for grabbing XInput2 devices
       ("linux-pam" ,linux-pam)
       ;; loginctl (part of systemd) is an optional run-time-dependency
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtx11extras" ,qtx11extras)
       ("solid" ,solid)
       ("wayland" ,wayland)
       ("xcb-util-keysyms" ,xcb-util-keysyms)))
    (home-page "https://cgit.kde.org/kscreenlocker.git")
    (synopsis "Secure lock screen architecture")
    (description "Library and components for secure lock screen
architecture.")
    (license license:gpl2+)))

(define-public ksshaskpass
  (package
    (name "ksshaskpass")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/ksshaskpass-" version ".tar.xz"))
      (sha256
       (base32 "1znhj8x8kag1jrw0j1kfvqgprdayrcfbmawz2jap1ik2bjq7dp81"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
       ("ki18n" ,ki18n)
       ("kwallet" ,kwallet)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("qtbase" ,qtbase)))
    (home-page "https://cgit.kde.org/ksshaskpass.git")
    (synopsis "SSH-askpass for KDE")
    (description "Ksshaskpass is a front-end for ssh-add.  It stores the
password of the ssh-key in KWallet and uses kpassworddialog for asking for the
password.

Ksshaskpass is not meant to be executed directly.  GuixSD will tell ssh-add to
use ksshaskpass if the former is not associated to a terminal.")
    (license license:gpl2+)))

(define-public ksysguard
  (package
    (name "ksysguard")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/ksysguard-" version ".tar.xz"))
      (sha256
       (base32 "1qjqhqc23rbimz3qj8gr3dhp0griwgbiajhvjngh1jl55fb3q29j"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kconfig" ,kconfig)
       ("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kinit" ,kinit)
       ("kio" ,kio)
       ("kitemviews" ,kitemviews)
       ("knewstuff" ,knewstuff)
       ("knotifications" ,knotifications)
       ("kwindowsystem" ,kwindowsystem)
       ("libksysguard", libksysguard)
       ("lm-sensors" ,lm-sensors "lib")
       ("qtbase" ,qtbase)))
    (home-page "https://www.kde.org/applications/system/ksysguard/")
    (synopsis "Plasma process and performance monitor")
    (description "KSysGuard is a program to monitor various elements of your
system, or any other remote system with the KSysGuard daemon (ksysgardd)
installed.

It features a client/server architecture that allows monitoring of local as
well as remote hosts.  The graphical front end uses so-called sensors to
retrieve the information it displays.  A sensor can return simple values or
more complex information like tables.  For each type of information, one or
more displays are provided.  Displays are organized in worksheets that can be
saved and loaded independently from each other.  So, KSysGuard is not only a
simple task manager but also a very powerful tool to control large server
farms.

Currently the daemon has been ported to Linux, FreeBSD, Irix,
NetBSD, OpenBSD, Solaris and Tru64 with varying degrees of completion.")
    (license (list license:gpl2 license:gpl2+ license:gpl3)))) ;; KDE e.V.


(define-public kwallet-pam
  (package
    (name "kwallet-pam")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kwallet-pam-" version ".tar.xz"))
      (sha256
       (base32 "145daahh8qjpbfcvjk2zyd6k3sr22npgnv3n23j9aim75qiwz1ac"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f ;; no make target 'test'
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-socat-path
           (lambda* (#:key inputs #:allow-other-keys)
             ;; Always install into /lib and not into /lib64.
             (substitute* "pam_kwallet_init"
               ((" socat ")
                (string-append " " (assoc-ref inputs "socat")
                               "/bin/socat ")))
             #t)))))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("libgcrypt" ,libgcrypt)
       ("pam" ,linux-pam)
       ("socat" ,socat)))
    (home-page "https://cgit.kde.org/kwallet-pam.git")
    (synopsis "PAM module for KWallet")
    (description "KWallet (KDE Frameworks 5) integration with PAM")
    (license license:lgpl2.1+)))

(define-public kwayland-integration
  (package
    (name "kwayland-integration")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kwayland-integration-" version ".tar.xz"))
      (sha256
       (base32 "1qhkrs8md36z5gndkm88pyv6mspqsdsdavjz8klfwfv1hii6qyds"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'check)
         (add-after 'install 'check-after-install
           (assoc-ref %standard-phases 'check))
         (add-before 'check-after-install 'check-setup
           (lambda* (#:key outputs #:allow-other-keys)
             ;; make Qt render "offscreen", required for tests
             (setenv "QT_QPA_PLATFORM" "offscreen")
             (setenv "QT_PLUGIN_PATH"
                     (string-append (assoc-ref outputs "out")
                                    "/lib/qt5/plugins:"
                                    (getenv "QT_PLUGIN_PATH")))
             #t)))))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("qtwayland" ,qtwayland))) ;; required for the tests
    (inputs
     `(("kidletime" ,kidletime)
       ("kwayland" ,kwayland)
       ("kwindowsystem" ,kwindowsystem)
       ("qtbase" ,qtbase)))
    (home-page "https://cgit.kde.org/kwayland-integration.git")
    (synopsis "KWayland runtime integration plugins")
    (description "Provides integration plugins for various KDE Frameworks for
Wayland")
    (license license:lgpl3+))) ; KDE e.V.

(define-public kwin
  (package
    (name "kwin")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kwin-" version ".tar.xz"))
      (sha256
       (base32 "0ld1pclni1axrh7jww3gxlfwkbjsfbqb9z7gygj2ff3nmc6khgfm"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     '(#:tests? #f ;; TODO 35/77 tests fail – even with the phases below
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch
           (lambda* (#:key inputs #:allow-other-keys)
             ;; follow symlinks - taken from NIX
             (substitute* "plugins/kdecorations/aurorae/src/aurorae.cpp"
               (("^(\\s*QDirIterator it\\(path, QDirIterator::Subdirectories)(\\);)" _ a b)
                (string-append a " | QDirIterator::FollowSymlinks " b)))
             ;; hard-code path to xwayland binary - taken from NIX
             ;; FIXME: This will install xorg-server-xwayland on all systems
             ;; using kwin :-(
             (substitute* "main_wayland.cpp"
               (("^(\\s*m_xwaylandProcess->setProgram\\(QStringLiteral\\(\")Xwayland(\"\\)\\);)" _ a b)
                (string-append a (assoc-ref inputs "xorg-server-xwayland")
                               "/bin/Xwayland" b)))
             #t))
         (add-after 'install 'check
           ;;(assoc-ref %standard-phases 'check))
           (lambda _
             (invoke "ctest" ".")))
         (add-before 'check 'check-setup
           (lambda* (#:key outputs #:allow-other-keys)
             (setenv "HOME" (getcwd))
             (setenv "XDG_RUNTIME_DIR" "..")
             (setenv "DBUS_FATAL_WARNINGS" "0")
             (setenv "CTEST_OUTPUT_ON_FAILURE" "1")
             ;; auprobieren, ob das was bringt:
             (setenv "XDG_DATA_DIRS"
                     (string-append (assoc-ref outputs "out") "/share:"
                                    (getenv "XDG_DATA_DIRS")))
             (setenv "QT_PLUGIN_PATH"
                     (string-append (assoc-ref outputs "out") "/lib/qt5/plugins:"
                                    (getenv "QT_PLUGIN_PATH")))
             ;; The test suite requires a running X server.
             (system "Xvfb :98 -screen 0 640x480x24 &")
             (setenv "DISPLAY" ":98")
             #t))
         (add-after 'install 'add-symlinks
           ;; Some package(s) refer to these service types by the wrong name.
           ;; I would prefer to patch those packages, but I cannot find them!
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((kst5 (string-append (assoc-ref outputs "out")
                                        "/share/kservicetypes5/")))
               (symlink (string-append kst5 "kwineffect.desktop")
                        (string-append kst5 "kwin-effect.desktop"))
               (symlink (string-append kst5 "kwinscript.desktop")
                        (string-append kst5 "kwin-script.desktop"))))))))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)
       ("qttools" ,qttools)
       ;; the remaining ones are required native for running the tests
       ("dbus" ,dbus)
       ("kwayland-integration" ,kwayland-integration)
       ("kwindowsystem" ,kwindowsystem)
       ("qtwayland" ,qtwayland)
       ("xorg-server" ,xorg-server)
       ("xorg-server-xwayland" ,xorg-server-xwayland)))
    (inputs
     `(("breeze" ,breeze)
       ("fontconfig" ,fontconfig)
       ("freetype" ,freetype)
       ("kactivities" ,kactivities)
       ("kcmutils" ,kcmutils)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kdeclarative" ,kdeclarative)
       ("kdecoration" ,kdecoration)
       ("kglobalaccel" ,kglobalaccel)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kidletime" ,kidletime)
       ("kinit" ,kinit)
       ("kio" ,kio)
       ("knewstuff" ,knewstuff)
       ("knotifications" ,knotifications)
       ("kpackage" ,kpackage)
       ("kscreenlocker" ,kscreenlocker)
       ("kservice" ,kservice)
       ("ktextwidgets" ,ktextwidgets)
       ("kwayland" ,kwayland)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kxmlgui" ,kxmlgui)
       ("libdrm" ,libdrm)
       ("libepoxy" ,libepoxy)
       ("libice" ,libice) ;; missing in CMakeList.txt
       ("libinput" ,libinput)
       ("libsm" ,libsm) ;; missing in CMakeList.txt
       ("libxi" ,libxi)
       ("libxkbcommon" ,libxkbcommon)
       ("plasma-framework" ,plasma-framework)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtmultimedia" ,qtmultimedia)
       ("qtscript" ,qtscript)
       ("qtsensors" ,qtsensors)
       ("qtx11extras" ,qtx11extras)
       ("wayland" ,wayland)
       ("xcb-util-cursor" ,xcb-util-cursor)
       ("xcb-util-image" ,xcb-util-image)
       ("xcb-util-keysyms" ,xcb-util-keysyms)
       ;; TODO: optional feature: libhybris allows to run bionic-based HW
       ;; adaptations in glibc systems.
       ("xcb-util-wm" ,xcb-util-wm))) ; for icccm:, optional
    (home-page "http://community.kde.org/KWin")
    (synopsis "KDE Plasma 5 Window Manager")
    (description " KWin is the default window manager for the KDE Plasma
Desktop.  It gives you complete control over your windows, making sure they're
not in the way but aid you in your task.  It paints the window decoration, the
bar on top of every window with (configurable) buttons like close, maximize
and minimize.  It also handles placing of windows and switching between
them.")
    (license license:gpl2+)))

(define-public kwrited
  (package
    (name "kwrited")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/kwrited-" version ".tar.xz"))
      (sha256
       (base32 "150nhjk4vcigs2r2bxqk309g81lxpnkkv8l44hiyivcbmwvc3aya"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("ki18n" ,ki18n)
       ("knotifications" ,knotifications)
       ;; FIXME: kwrited is currently build as an executable, since we don't
       ;; have utempter yet and thus kpty is build without utempter-support.
       ("kpty" ,kpty)
       ("qtbase" ,qtbase)))
    (home-page "https://cgit.kde.org/kwrited.git")
    (synopsis "Read and write console output to X")
    (description "Kwrited captures console output (e.g. broadcast messages)
and prints it in a X window.")
    (license license:gpl2+)))

(define-public milou
  (package
    (name "milou")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/milou-" version ".tar.xz"))
      (sha256
       (base32 "0rhgj10l2iik1mgnv2bixxqjyc3pl731bs1bqz9gsa3wiazspwrv"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    ;; TOOO: warnings during generation of metainfo for org.kde.milou: Package
    ;; type "Plasma/Applet" not found
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
       ("kdeclarative" ,kdeclarative)
       ("kwindowsystem" ,kwindowsystem)
       ("ki18n" ,ki18n)
       ("krunner" ,krunner)
       ("kservice" ,kservice)
       ("plasma-framework" ,plasma-framework)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtscript" ,qtscript)
       ("qtbase" ,qtbase)))
    (home-page "https://cgit.kde.org/milou.git")
    (synopsis "Dedicated search plasmoid built on top of Baloo")
    (description "Milou can also be used as an alternative to KRunner, and
does provide application launching.  The main difference is that it
concentrates more on searching.")
    (license license:gpl3))) ;; KDE e.V.

(define-public oxygen
  (package
    (name "oxygen")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/oxygen-" version ".tar.xz"))
      (sha256
       (base32 "0wm2mngh0gb0lqvx8g82ml2sdv0kbkx14mpb8c6aw3hslcwma7yd"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("kde-frameworkintegration" ,kde-frameworkintegration)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kiconthemes" ,kiconthemes)
       ("kguiaddons" ,kguiaddons)
       ("ki18n" ,ki18n)
       ("kservice" ,kservice)
       ("kwayland" ,kwayland)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("kcmutils" ,kcmutils)
       ("kdecoration" ,kdecoration)
       ("qtx11extras" ,qtx11extras)))
    (home-page "https://cgit.kde.org/oxygen.git")
    (synopsis "Widget and icon theme")
    (description "Plasma and Qt widget style and window decorations for Plasma
5 and KDE 4.")
    ;; Parts of the code is Expat licensed, other parts GPL-3+. The artwork is
    ;; under some different licenses.
    (license (list license:expat license:lgpl3+ ;; KDE e.V.
                   license:gpl2+ license:lgpl2.1))))

(define-public plasma-desktop
  (package
    (name "plasma-desktop")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/plasma-desktop-" version ".tar.xz"))
      (sha256
       (base32 "14isrq3n9lm1nzmyv8zdgq6pwnv2zmg4dwxyp7fvqjxfls8851vp"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     '(#:phases
       (modify-phases %standard-phases
         (add-after `unpack `fix-paths
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "kcms/dateandtime/helper.cpp"
               ;; define path to bin/hwclock
               ;; TODO: Rethink! NIX defines the path, but this leads to
               ;; util-linux being an additional requirement. We can just
               ;; leave this off and let KCM search $PATH
               (("^\\s*static const QString exePath = QStringLiteral")
                "// ")
               (("(^\\s*QString hwclock = )KStandardDirs::findExe.*" l prefix)
                (string-append prefix "QLatin1String(\""
                               (assoc-ref inputs "util-linux")
                               "/bin/hwclock\");"))
               ;; define path to zoneinfo
               ;; TODO: nix also has a patch to honor $TZDIR
               (("\"/usr/share/zoneinfo/\"")
                (string-append "\"" (assoc-ref inputs "tzdata")
                               "/share/zoneinfo/\"")))
             (substitute* "kcms/keyboard/iso_codes.h"
               ;; define path to iso-codes
               (("\"/usr/share/xml/iso-codes\"")
                (string-append "\"" (assoc-ref inputs "iso-codes")
                               "/share/xml/iso-codes\"")))
               #t))
         (add-after 'unpack 'patch-qml-import-path
           (lambda _
             (substitute*
              '("applets/pager/package/contents/ui/main.qml"
                "containments/desktop/package/contents/ui/FolderView.qml"
                "containments/desktop/package/contents/ui/main.qml"
                "containments/panel/contents/ui/main.qml")
              (("^import \"(utils|FolderTools|LayoutManager).js\" as " line mod)
               (string-append "import \"../code/" mod ".js\" as ")))
             #t))
         (add-after 'unpack 'patch-includes
           ;; TODO: Is this correct? Why do other distributions not need this?
           (lambda _
             (substitute*
              '("kcms/touchpad/src/backends/x11/xlibbackend.cpp"
                "kcms/touchpad/src/backends/x11/xlibtouchpad.cpp")
              (("^#include <xserver-properties.h>")
               "#include <xorg/xserver-properties.h>"))
             #t))
         (delete 'check)
         (add-after 'install 'check
           (assoc-ref %standard-phases 'check))
         (add-before 'check 'check-setup
           (lambda* (#:key outputs #:allow-other-keys)
             (setenv "HOME" (getcwd))
             (setenv "DBUS_FATAL_WARNINGS" "0")
             (setenv "CTEST_OUTPUT_ON_FAILURE" "1")
             (setenv "QT_PLUGIN_PATH"
                     (string-append (assoc-ref outputs "out") "/lib/qt5/plugins:"
                                    (getenv "QT_PLUGIN_PATH")))
             ;; The test 'keyboard-geometry_parser' queries for device
             ;; information, for this it requires a running X server.
             (system "Xvfb :98 -screen 0 640x480x24 &")
             (setenv "DISPLAY" ":98")
             ;; blacklist failing test-functions TODO: make them pass
             (with-output-to-file "kcms/keyboard/tests/BLACKLIST"
               (lambda _
                 ;; In kcm-keyboard-flags_test
                 (display "[testRules]\n*\n")))
             (with-output-to-file "kcms/kfontinst/kio/autotests/BLACKLIST"
               (lambda _
                 ;; In kcm-keyboard-flags_test
                 (display "[testDirLister]\n*\n")))
             #t)))))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("dbus" ,dbus) ; required for running the tests
       ("kdoctools" ,kdoctools)
       ("pkg-config" ,pkg-config)
       ("xorg-server" ,xorg-server))) ; required for running the tests
    (inputs
     ;; TODO: Still some unknown property types, e.g for key "X-Plasma-API",
     ;;  "X-KDE-ParentApp", "X-Plasma-RemoteLocation", "X-Plasma-MainScript".
     ;; TODO: XCB-XINPUT missing
     ;; TODO: Add more optional inputs: IBUS, GIO, GObject, SCIM, GLIB2
     ;; TODO: "Recommended" input AppStreamQt.
     `(("attica" ,attica)
       ("baloo" ,baloo) ; recommended
       ("boost" ,boost) ; optional
       ("breeze" ,breeze)
       ("eudev", eudev) ; optional
       ("fontconfig" ,fontconfig) ;; package fontutils
       ("iso-codes" ,iso-codes) ; for path-substitution (see phases), required for testing
       ("kactivities" ,kactivities)
       ("kactivities-stats" ,kactivities-stats)
       ("kauth" ,kauth)
       ("kcmutils" ,kcmutils)
       ("kconfig" ,kconfig)
       ("kdbusaddons" ,kdbusaddons)
       ("kdeclarative" ,kdeclarative)
       ("kded" ,kded) ; not checked in CmakeList
       ("kdelibs4support" ,kdelibs4support)
       ("kemoticons" ,kemoticons)
       ("kglobalaccel" ,kglobalaccel)
       ("ki18n" ,ki18n)
       ("kitemmodels" ,kitemmodels)
       ("knewstuff" ,knewstuff)
       ("knotifications" ,knotifications)
       ("knotifyconfig" ,knotifyconfig)
       ("kpeople" ,kpeople)
       ("krunner" ,krunner)
       ("kscreenlocker" ,kscreenlocker)
       ("kwallet" ,kwallet)
       ("kwin" ,kwin)
       ("libcanberra_kde" , libcanberra) ; optional
       ("libxcursor", libxcursor) ; not checked in CMakelist
       ("libxi" ,libxi) ;; X11-Xinput, for kcms/input/
       ("libxft" ,libxft) ; feature
       ("libxkbfile" ,libxkbfile)
       ("phonon" ,phonon)
       ("plasma-framework" ,plasma-framework)
       ("plasma-workspace" ,plasma-workspace)
       ("pulseaudio" ,pulseaudio) ; optional
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtsvg" ,qtsvg)
       ("qtx11extras" ,qtx11extras)
       ("tzdata" ,tzdata) ; for path-substitution (see phases)
       ("util-linux" ,util-linux) ; for path-substitution (see phases)
       ("xcb-util" ,xcb-util)
       ("xcb-util-image" ,xcb-util-image)
       ("xf86-input-evdev" ,xf86-input-evdev)
       ("xf86-input-synaptics" ,xf86-input-synaptics)
       ("xkeyboard-config" ,xkeyboard-config)
       ("xorg-server" ,xorg-server))) ;; xserver-properties.h, not checked in CmakeList
    (home-page "https://cgit.kde.org/plasma-desktop.git")
    (synopsis "Plasma 5 application workspace components")
    (description "Tools and widgets for the desktop")
    (license (list license:lgpl2.0+ license:gpl2+ license:fdl1.2+))))

(define-public plasma-integration
  (package
    (name "plasma-integration")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/plasma-integration-" version ".tar.xz"))
      (sha256
       (base32 "0j57ra79p5lkj81d05hhb87mrxgyj6qikkpzcb0p2dr2x8cmkng2"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'check)
         (add-after 'install 'check
           (lambda _
             ;; Exclude a "Received signal" test TODO: Make this test pass
             (invoke "ctest" "."
                     "-E" "frameworkintegration-kfiledialog_unittest")))
         (add-before 'check 'check-setup
           (lambda* (#:key outputs #:allow-other-keys)
             (setenv "HOME" (getcwd))
             (setenv "XDG_RUNTIME_DIR" "..")
             (setenv "DBUS_FATAL_WARNINGS" "0")
             (setenv "CTEST_OUTPUT_ON_FAILURE" "1")
             (setenv "QT_PLUGIN_PATH"
                     (string-append (assoc-ref outputs "out") "/lib/qt5/plugins:"
                                    (getenv "QT_PLUGIN_PATH")))
             ;; The test suite requires a running X server.
             (system "Xvfb :98 -screen 0 640x480x24 &")
             (setenv "DISPLAY" ":98")
             ;; blacklist failing test-functions TODO: make them pass
             (with-output-to-file "autotests/BLACKLIST"
               (lambda _
                 ;; In frameworkintegration-kdeplatformtheme_unittest
                 (display "[testPlatformHints]\n*\n")
                 (display "[testPlatformPalette]\n*\n")
                 (display "[testPlatformHintChanges]\n*\n")
                 ;; In frameworkintegration-kfiledialogqml_unittest
                 (display "[testShowDialogParentless]\n*\n")
                 (display "[testShowDialogWithParent]\n*\n")))
             #t)))))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("dbus" ,dbus)
       ("xorg-server" ,xorg-server) ; required for running the tests
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("breeze" ,breeze)
       ;; Other distributions do not install these fonts as a dependency of
       ;; this package.
       ;;("font-google-noto" ,font-google-noto) ; runtime dependency
       ;;("font-hack" ,font-hack) ; runtime dependency
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("knotifications" ,knotifications)
       ("kwayland" ,kwayland)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("libxcursor" ,libxcursor)
       ("perl" ,perl) ; for the kconf_update scripts
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative) ; for generating auto-tests
       ("qtquickcontrols" ,qtquickcontrols) ; for running the tests
       ("qtquickcontrols2" ,qtquickcontrols2)
       ("qtx11extras" ,qtx11extras)
       ("xcb-util" ,xcb-util)))
    (home-page "https://cgit.kde.org/plasma-integration.git")
    (synopsis "Integration of Qt application with KDE workspaces")
    (description "Plasma Framework Integration is a set of plugins responsible
for better integration of Qt applications when running on a KDE Plasma
workspace.  Applications do not need to link to this directly.")
    (license license:lgpl3+))) ;; KDE e.V.

(define-public plasma-nm
  (package
    (name "plasma-nm")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/plasma-nm-" version ".tar.xz"))
      (sha256
       (base32 "1z8f5iybgra72vhpiayiwpysvv2z8x2r5xal8rhgf7y24xcjwxmi"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    ;; TODO: Still some unknown property types, e.g for key
    ;; "X-KDE-ModuleType", "X-NetworkManager-Services"
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)
       ("qttools" ,qttools)))
    (inputs
     ;; TODO: Think about enabling openconnect
     `(("kcompletion" ,kcompletion)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("kdeclarative" ,kdeclarative)
       ("kded" ,kded) ;; required for property type definitions
       ("kdelibs4support" ,kdelibs4support)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kinit" ,kinit)
       ("kio" ,kio)
       ("kitemviews" ,kitemviews)
       ("knotifications" ,knotifications)
       ("kservice" ,kservice)
       ("kwallet" ,kwallet)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("kxmlgui" ,kxmlgui)
       ("mobile-broadband-provider-info" ,mobile-broadband-provider-info)
       ("modemmanager-qt" ,modemmanager-qt)
       ("networkmanager-qt" ,networkmanager-qt)
       ("plasma-framework" ,plasma-framework)
       ("qca" ,qca)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("solid" ,solid)))
    (home-page "https://cgit.kde.org/plasma-nm.git")
    (synopsis "Plasma5 NetworkManager applet")
    (description "This package contains the Network Management plasma widget
which aims to provide a fully featured GUI for managing networks.  Currently,
the only supported backend is NetworkManager.  Support for wired, wireless,
mobile and VPN networks are provided.  The widget is exclusively written for
KDE Plasma workspaces and it is not supposed to work in other environments.

In order to start using the widget, look for 'Network Management' in the 'Add
Widgets' dialog of the Plasma workspace you're using.")
    (license license:gpl3))) ;; KDE e.V.

(define-public plasma-pa
  (package
    (name "plasma-pa")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/plasma-pa-" version ".tar.xz"))
      (sha256
       (base32 "0p54x4zr3w009nn7g00qmxh7xil35x7b48d0l0flz5d7hvkk6nd8"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    ;; TODO: Still some unknown property types, e.g for key
    ;;  "X-KDE-ParentApp", "X-Plasma-API", "X-DocPath", "X-KDE-Keywords".
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("gconf" ,gconf)
       ("kcmutils" ,kcmutils) ;; required for property type definitions
       ("kcoreaddons" ,kcoreaddons)
       ("kdeclarative" ,kdeclarative)
       ("kglobalaccel" ,kglobalaccel)
       ("kwindowsystem" ,kwindowsystem)
       ("ki18n" ,ki18n)
       ("libcanberra" ,libcanberra)
       ("perl" ,perl) ; for the kconf_update scripts
       ("plasma-framework" ,plasma-framework)
       ("pulseaudio" ,pulseaudio)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)))
    (home-page "https://cgit.kde.org/plasma-pa.git")
    (synopsis "Plasma 5 Volume controller")
    (description "Plasma applet for audio volume management using
PulseAudio.")
    (license license:gpl3))) ;; KDE e.V.

(define-public plasma-sdk
  (package
    (name "plasma-sdk")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/plasma-sdk-" version ".tar.xz"))
      (sha256
       (base32 "1x8hq343xzwlcsdvf0jy0qgn64xw8l11lawhknbjrf90qq58axga"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'check 'check-setup
           (lambda _
             ;; make Qt render "offscreen", required for tests
             (setenv "QT_QPA_PLATFORM" "offscreen")
             (setenv "HOME" (getcwd))
             #t)))))
    ;; TODO: Still some unknown property types, e.g for key
    ;; "X-KDE-ParentApp", "X-DocPath", "X-KDE-Keywords".
    ;; TOOO: warnings during generation of metainfo for
    ;; org.kde.plasma.plasmoidviewershell: Package type "Plasma/Shell" not
    ;; found
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)
       ("qttools" ,qttools)))
    (inputs
     `(("grantlee" ,grantlee)
       ("karchive" ,karchive)
       ("kcmutils" ,kcmutils)
       ("kcompletion" ,kcompletion)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("kdbusaddons" ,kdbusaddons)
       ("kdeclarative" ,kdeclarative)
       ("kdelibs4support" ,kdelibs4support)
       ("kdevplatform" ,kdevplatform)
       ("kguiaddons" ,kguiaddons)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kirigami", kirigami) ;; runtime dependency
       ("kitemmodels" ,kitemmodels)
       ("knewstuff" ,knewstuff)
       ("knotifications" ,knotifications)
       ("knotifyconfig" ,knotifyconfig)
       ("kservice" ,kservice)
       ("ktexteditor" ,ktexteditor)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("kxmlgui" ,kxmlgui)
       ("plasma-framework" ,plasma-framework)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtsvg" ,qtsvg)
       ("qtwebkit" ,qtwebkit)
       ("threadweaver" ,threadweaver)))
    (home-page "https://community.kde.org/Plasma/DeveloperGuide")
    (synopsis "Development tools for Plasma 5 components")
    (description "Plasma SDK contains the following tools for Plasma-related
development:
@enumerate
@item CuttleFish - icon theme browser
@item EngineExplorer - tool to browse and interact with data engines
@item PlasmoidViewer - an isolated Plasma environment for testing applets
@item ThemeExplorer - shows all components of a widget theme
@end enumerate")
    (license (list license:gpl2 license:gpl2+ license:gpl3+))))


(define plasma-tests
  (package
    (name "plasma-tests")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/plasma-tests-" version ".tar.xz"))
      (sha256
       (base32 "00nm0d0c4zccbwnhy8sc1qb4sf7bs5vfky3n7lihwyng3syqwz3d"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    ;; TODO: Add input appstreamcli
    (home-page "https://cgit.kde.org/plasma-tests.git")
    (synopsis "Integration-tests for the Plasma workspace")
    (description "Distributions should not package it, but might want to run
it as part of their Plasma builds.")
    ;; No license to be found in the archive. Since most parts of KDE are
    ;; GPL2+, I assume that here, too.
    (license license:gpl2+)))

(define-public plasma-workspace
  (package
    (name "plasma-workspace")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/plasma-workspace-" version ".tar.xz"))
      (sha256
       (base32 "1qcmw60lyp966rhvw9raaqrvxdv09pr8zc7x3fx1vpm9kphh3lv3"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (arguments
     '(#:configure-flags '("-Wno-dev") ;; too many dev-warnings, silence them
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-qml-import-path
           (lambda* (#:key outputs #:allow-other-keys)
             (substitute*
              '("applets/batterymonitor/package/contents/ui/BatteryItem.qml"
                "applets/batterymonitor/package/contents/ui/PopupDialog.qml"
                "applets/batterymonitor/package/contents/ui/batterymonitor.qml"
                "applets/batterymonitor/package/contents/ui/CompactRepresentation.qml"
                "applets/analog-clock/contents/ui/analogclock.qml"
                "applets/lock_logout/contents/ui/lockout.qml"
                "applets/notifications/package/contents/ui/main.qml")
              (("^import \"(logic|data|uiproperties).js\" as " line mod)
               (string-append "import \"../code/" mod ".js\" as ")))
             (substitute* "startkde/kstartupconfig/kstartupconfig.cpp"
               (("kdostartupconfig5") ;; TODO: Is this a good pattern?
                (string-append (assoc-ref outputs "out")
                               "/bin/kdostartupconfig5")))
             #t))
         (add-after 'unpack 'patch-exec-paths
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute*
                 '("startkde/startkde.cmake"
                   "startkde/startplasma.cmake")
               (("@CMAKE_INSTALL_FULL_LIBEXECDIR_KF5@/start_kdeinit_wrapper")
                (string-append (assoc-ref inputs "kinit")
                               "/lib/libexec/kf5/start_kdeinit_wrapper")))
             #t))
         (replace 'check
           ;; TODO: Make this test pass. check-after-install, setting
           ;; QT_PLUGIN_PATH, starting a X11-server did not suffice to make
           ;; testdesktop pass.
           ;; launchertasksmodeltest fails since it relies on .desktop-files
           ;; from installed dolphin and konquerer, see
           ;; <https://bugs.kde.org/show_bug.cgi?id=386458>
           (lambda _
             (invoke "ctest" "." "-E" "testdesktop|launchertasksmodeltest")))
         (add-before 'check 'check-setup
           (lambda _
             ;; make Qt render "offscreen", required for tests
             (setenv "QT_QPA_PLATFORM" "offscreen")
             (setenv "HOME" (getcwd))
             (setenv "CTEST_OUTPUT_ON_FAILURE" "1")
             #t))
         ;; TODO: Nix removes this files, should we also do this?
         ;; (add-after 'install 'remove-startup-files
         ;;   (lambda* (#:key outputs #:allow-other-keys)
         ;;     (let ((out (assoc-ref outputs "out")))
         ;;       (delete-file (string-append out "/bin/startkde"))
         ;;       (delete-file (string-append out "/bin/startplasmacompositor"))
         ;;       (delete-file (string-append out "/lib/libexec/startplasma"))
         ;;       (delete-file-recursively
         ;;        (string-append out "/share/wayland-sessions")))))
         )))
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)
       ("kdoctools" ,kdoctools)))
    (propagated-inputs
     `(("iso-codes" ,iso-codes))); run-time dependency
    ;; TODO: Warning at /gnu/store/…-kpackage-5.34.0/…/KF5PackageMacros.cmake:
    ;;   warnings during generation of metainfo for org.kde.breeze.desktop:
    ;;   Package type "Plasma/LookAndFeel" not found
    ;; TODO: Warning at /gnu/store/…-kpackage-5.37.0/…/KF5PackageMacros.cmake:
    ;;  warnings during generation of metainfo for org.kde.image:
    ;;  Package type "Plasma/Wallpaper" not found
    ;; TODO: Still some unknown property types, e.g for key "X-KDE-ParentApp",
    ;; "X-Plasma-RemoteLocation", "X-Plasma-EngineName",
    ;; "X-Plasma-MainScript".
    (inputs
     `(;; TODO: Optional: AppStreamQt, Qalculate, libgps
       ("baloo" ,baloo)
       ("kactivities" ,kactivities)
       ("kcmutils" ,kcmutils) ; nicht in NIX
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("kdeclarative" ,kdeclarative)
       ("kded" ,kded) ;
       ("kdelibs4support" ,kdelibs4support)
       ("kdesu" ,kdesu)
       ("kglobalaccel" ,kglobalaccel)
       ("kholidays" ,kholidays) ;; optional, for Plasma Calendar plugin
       ("ki18n" ,ki18n)
       ("kidletime" ,kidletime)
       ("kinit" ,kinit) ;; required by startkde, not listed as a requirement
       ("kjs" ,kjs)
       ("kjsembed" ,kjsembed)
       ("knewstuff" ,knewstuff)
       ("knotifyconfig" ,knotifyconfig)
       ("kpackage" ,kpackage)
       ("krunner" ,krunner)
       ("kscreenlocker" ,kscreenlocker)
       ("ksysguard" ,ksysguard)
       ("ktexteditor" ,ktexteditor)
       ("ktextwidgets" ,ktextwidgets)
       ("kwallet" ,kwallet)
       ("kwayland" ,kwayland)
       ("kwin" ,kwin)
       ("kxmlrpcclient" ,kxmlrpcclient)
       ("libksysguard" ,libksysguard)
       ("libsm" ,libsm)
       ("libxrender" ,libxrender)
       ("libxtst" ,libxtst) ; not listed as a requirement
       ("networkmanager-qt" ,networkmanager-qt)
       ("phonon" ,phonon)
       ("plasma-framework" ,plasma-framework)
       ("prison" ,prison)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtscript" ,qtscript)
       ("qtx11extras" ,qtx11extras)
       ("solid" ,solid)
       ("xcb-util" ,xcb-util)
       ("xcb-util-image" ,xcb-util-image)
       ("xcb-util-keysyms" ,xcb-util-keysyms)
       ("zlib" ,zlib)))
    (home-page "https://cgit.kde.org/plasma-tests.git")
    (synopsis "Plasma workspace components for KF5")
    (description "Workspaces provide support for KDE Plasma Widgets,
integrated search, hardware management and a high degree of customizability.")
    ;; Parts of the code is Expat licensed, other parts GPL-3+ and even other
    ;; parts are LGPL2.1+. The artwork is under some different licenses.
    (license (list license:expat license:lgpl3+ ;; KDE e.V.
                   license:gpl2 license:lgpl2.1 license:gpl2+))))

(define-public plasma-workspace-wallpapers
  (package
    (name "plasma-workspace-wallpapers")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/plasma-workspace-wallpapers-" version ".tar.xz"))
      (sha256
       (base32 "1wbnm6bzvgx2ssig4dk3plhrsjiw3lq1yhr2dfga6vvlyi6wg9mg"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (home-page "http://vdesign.kde.org/")
    (synopsis "Wallpapers for Plasma workspace")
    (description "This package contains the default wallpapers for the Plasma
workspace.")
    ;; The archive includes the text of both gpl2 and lgpl3, but the files are
    ;; actually all lgpl3 (according to the metadata.desktop files).
    (license license:lgpl3)))

(define-public polkit-kde-agent-1
  (package
    (name "polkit-kde-agent-1")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/polkit-kde-agent-1-" version ".tar.xz"))
      (sha256
       (base32 "00f05ii3www8knn2ycgkc6izc8ydb3vjy4f657k38hkzl2sjnhl6"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)))
    (inputs
     `(("kcoreaddons" ,kcoreaddons)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("kwindowsystem" ,kwindowsystem)
       ("polkit-qt5" ,polkit-qt)
       ("qtbase" ,qtbase)))
    (home-page "https://cgit.kde.org/polkit-kde-agent-1.git")
    (synopsis "PolicyKit authentication agent dialogs for KDE")
    (description "PolicyKit is an application-level toolkit for defining and
handling the policy that allows unprivileged processes to speak to privileged
processes.  It is a framework for centralizing the decision making process
with respect to granting access to privileged operations (like calling the HAL
Mount() method) for unprivileged (desktop) applications.

PolicyKit-Kde provides a D-Bus session bus service that is used to bring up
authentication dialogs used for obtaining privileges.")
    (license license:gpl2+)))

(define-public powerdevil
  (package
    (name "powerdevil")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/powerdevil-" version ".tar.xz"))
      (sha256
       (base32 "1k7ilcvm5nvx6sd43j0djar9ay6ag84g4m8f420yf7q4yryp76yn"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("bluez-qt" ,bluez-qt)
       ("kactivities" ,kactivities)
       ("kauth" ,kauth)
       ("kconfig" ,kconfig)
       ("kdbusaddons" ,kdbusaddons)
       ("eudev", eudev)
       ("kdelibs4support" ,kdelibs4support)
       ("kglobalaccel" ,kglobalaccel)
       ("ki18n" ,ki18n)
       ("kidletime" ,kidletime)
       ("plasma-workspace" ,plasma-workspace)
       ("kio" ,kio)
       ("knotifyconfig" ,knotifyconfig)
       ("kwayland" ,kwayland)
       ("libkscreen" ,libkscreen)
       ("networkmanager-qt" ,networkmanager-qt)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)
       ("qtx11extras" ,qtx11extras)
       ("solid" ,solid)
       ("xcb-util-keysyms" ,xcb-util-keysyms)))
    (home-page "https://cgit.kde.org/powerdevil.git")
    (synopsis "Manages the power consumption in Plasma")
    (description "Power Devil sets and manages the power consumption according
to your settings.")
    (license license:gpl2+)))

(define-public sddm-kcm
  (package
    (name "sddm-kcm")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/sddm-kcm-" version ".tar.xz"))
      (sha256
       (base32 "122g83ajh0xqylvmicrhgw0fm8bmzpw26v7fjckfk9if5zqzk8ch"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("karchive" ,karchive)
       ("kauth" ,kauth)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("ki18n" ,ki18n)
       ("kio" ,kio)
       ("knewstuff" ,knewstuff)
       ("kxmlgui" ,kxmlgui)
       ("libxcursor" ,libxcursor) ;; Missing as dependency
       ("qtbase" ,qtbase)
       ("xcb-util-image" ,xcb-util-image)
       ("qtdeclarative" ,qtdeclarative)
       ("qtx11extras" ,qtx11extras)))
    (home-page "https://cgit.kde.org/sddm-kcm.git")
    (synopsis "SDDM configuration module for KDE")
    (description "This is a System Settings configuration module for
configuring the SDDM Display Manager.")
    (license license:gpl2+)))

(define-public systemsettings
  (package
    (name "systemsettings")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/systemsettings-" version ".tar.xz"))
      (sha256
       (base32 "14029a3mf2d6cw87lyffnwy88yvj0n3jmi0glr69zwi8lmz0cbsv"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("kdoctools" ,kdoctools)))
    (inputs
     `(("kactivities" ,kactivities)
       ("kactivities-stats" ,kactivities-stats)
       ("kcmutils" ,kcmutils)
       ("kconfig" ,kconfig)
       ("kcrash" ,kcrash)
       ("kdbusaddons" ,kdbusaddons)
       ("kdeclarative" ,kdeclarative)
       ("khtml" ,khtml)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kitemviews" ,kitemviews)
       ("kjs" ,kjs)
       ("kpackage" ,kpackage)
       ("kparts" ,kparts)
       ("kservice" ,kservice)
       ("kwindowsystem" ,kwindowsystem)
       ("kirigami" ,kirigami)
       ("kxmlgui" ,kxmlgui)
       ("qtbase" ,qtbase)
       ("qtdeclarative" ,qtdeclarative)))
    (home-page "https://cgit.kde.org/systemsettings.git")
    (synopsis "Plasma System Settings")
    (description "Plasma System Settings is an improved user interface for
configuring the desktop and other aspects of the system.")
    (license license:gpl2+)))

(define-public user-manager
  (package
    (name "user-manager")
    (version "5.13.5")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "mirror://kde/stable/plasma/" version
                          "/user-manager-" version ".tar.xz"))
      (sha256
       (base32 "12550xvl084rab0y331r8dm3qwpcvm83k3j02gxrwrigv1vckas8"))))
    (properties `((tags . '("Desktop" "KDE" "Plasma"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("extra-cmake-modules" ,extra-cmake-modules)
       ("pkg-config" ,pkg-config)))
    (inputs
     `(("kauth" ,kauth)
       ("kcmutils" ,kcmutils)
       ("kconfig" ,kconfig)
       ("kconfigwidgets" ,kconfigwidgets)
       ("kcoreaddons" ,kcoreaddons)
       ("ki18n" ,ki18n)
       ("kiconthemes" ,kiconthemes)
       ("kio" ,kio)
       ("kwidgetsaddons" ,kwidgetsaddons)
       ("libpwquality" ,libpwquality)
       ("qtbase" ,qtbase)))
    (home-page "https://cgit.kde.org/user-manager.git")
    (synopsis "User management tool for the Plasma workspace")
    (description "A simple system settings module to manage the users on your
system.")
    (license license:gpl2+)))
